import io from 'socket.io-client';
import { v4 as uuidv4 } from 'uuid';
import { TerminalsManager, TerminalEntry } from './terminalsManager';
import { EventEmitter } from 'events';

const defaultMultiplayerUri = process.env.SIDEKICK_NODE_ENV === 'dev' ? 'http://localhost:80' : 'http://35.223.17.119';
const defaultTerminalsPort = process.env.SIDEKICK_NODE_ENV === 'dev' ? 8011 : 8022;


class SessionsManager {
  private socket?: SocketIOClient.Socket;
  private uid?: string;
  private terminalsManager: TerminalsManager;
  private teamName?: string;

  sessionsEmitter = new EventEmitter();
  usersEmitter = new EventEmitter();
  connectionEmitter = new EventEmitter();

  hangingSessionIds: { [key: string]: boolean | undefined } = {};


  disconnect() {
    this.socket?.disconnect();
    this.socket = undefined;

    this.uid = undefined;
    this.teamName = undefined;
  }

  get connected() {
    return this.socket?.connected;
  }

  get terminals() {
    return this.terminalsManager.terminals;
  }

  constructor(private multiplayerUri: string = defaultMultiplayerUri, terminalsPort: number = defaultTerminalsPort) {
    this.terminalsManager = new TerminalsManager(terminalsPort);
    this.terminalsManager.cliEmitter.on('send_message', ({ socketId, receiverUsername, message }) => {
      if (this.socket) {
        this.socket.emit('send_message', { receiverUsername, message }, (response: any) => {
          // TODO:
          // TODO: Add type to response
          console.log('RESPONSE FROM SERVER', response);
          this.terminalsManager.respondToCLI(socketId, 200, response);
        });
      } else {
        this.terminalsManager.respondToCLI(socketId, 400, { response: 'Could not connect to the remote server. Socket not available.' });
      }
    });

  }

  markNextConnectedTerminal() {
    this.terminalsManager.markNextConnectedTerminal();
  }

  closePeerSessions() {
    this.terminalsManager.closeAllPeerSessions();
  }

  turnTerminalHighlightOn(terminalId: string, message: string, color: string) {
    return this.terminalsManager.turnTerminalHighlightOn(terminalId, message, color);
  }

  turnTerminalHighlightOff(terminalId: string) {
    return this.terminalsManager.turnTerminalHighlightOff(terminalId);
  }

  async getOnlineUsers() {
    if (this.socket) {
      return new Promise((resolve, reject) => {
        if (!this.socket) {
          return reject('Not connceted to the remote server');
        }
        this.socket.once('get_online_users', (uids: string[]) => {
          return resolve(uids);
        });
        this.socket.emit('get_online_users');
      });
    }
    return Promise.reject('Not connected to the remote server');
  }

  closePeerSessionBySessionId(sessionId: string) {
    const terminal = this.terminalsManager.terminals.findOne({ sessionId });
    if (terminal) {
      this.terminalsManager.exitPeerSession(terminal.terminalId);
    } else {
      console.log(`Cannot find terminal with an active session ${sessionId}`);
    }
  }

  closePeerSessionByTerminalId(terminalId: string) {
    this.terminalsManager.exitPeerSession(terminalId);
  }

  async updateEntry(terminalId: string) {
    await this.terminalsManager.updateTerminalEntry(terminalId);
  }

  getIsHanging(sessionId: string) {
    return this.hangingSessionIds[sessionId];
  }

  async checkTeammatesProgramsVersions(teammateUserID: string) {
    return new Promise((resolve, reject) => {
      if (this.socket) {
        this.socket.emit('check_programs_versions', teammateUserID, (error: any, result?: any) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(result);
          }
        });
      } else {
        return reject('Cannot find websocket connection to the remote server');
      }
    });
  }

  async createPeerSession(clientId: string, terminalId: string) {
    await this.terminalsManager.updateTerminalEntry(terminalId);
    const terminal = this.terminals.findOne({ terminalId });
    console.log('TerminalEntry', terminal);
    if (!terminal) {
      throw new Error('Cannot find terminal with id:' + terminalId);
    }
    if (!this.uid) {
      throw new Error('The host uid is not set');
    }
    if (this.socket) {
      const sessionId = uuidv4();
      this.terminalsManager.createPeerSession(terminalId, sessionId);

      this.socket.emit('peer_session_initialize', this.uid, clientId, sessionId, terminal);

      this.hangingSessionIds[sessionId] = true;

      setTimeout(() => {
        if (this.getIsHanging(sessionId)) {
          this.hangingSessionIds[sessionId] = undefined;
          console.log('auto hang out')
          this.closePeerSessionBySessionId(sessionId);
          this.sessionsEmitter.emit('rejected', clientId, sessionId);
          this.socket?.emit('peer_session_hangout', clientId, sessionId);
        } else {
          console.log('no auto hang out')
        }
      }, 20000);

      return sessionId;
    } else {
      throw new Error('Not connected to the websocket multiplayer server');
    }
  }

  acceptPeerSession(sessionId: string, terminalId: string) {
    if (this.socket) {
      this.hangingSessionIds[sessionId] = undefined;
      this.terminalsManager.joinPeerSession(terminalId, sessionId);
      this.socket.emit('peer_session_accept', sessionId);
    } else {
      throw new Error('Not connected to the websocket multiplayer server');
    }
  }

  rejectPeerSession(sessionId: string) {
    if (this.socket) {
      this.hangingSessionIds[sessionId] = undefined;
      this.socket.emit('peer_session_reject', sessionId);
    } else {
      throw new Error('Not connected to the websocket multiplayer server');
    }
  }

  async changeTeam(teamName: string) {
    try {
      const result = await new Promise((resolve, reject) => {
        if (this.socket) {
          this.socket.emit('change_team', { teamName }, (error: any, result?: any) => {
            if (error) {
              return reject(error);
            } else {
              return resolve(result);
            }
          });
        } else {
          return reject('Cannot find socket connection');
        }
      });
      this.teamName = teamName;
      return result;
    } catch (error) {
      console.error(error);
    }
  }

  async connect(uid: string) {
    this.disconnect();

    this.socket = io(`${this.multiplayerUri}/multiplayer`, {
      transportOptions: {
        polling: {
          extraHeaders: {
            uid,
          },
        },
      },
    });

    this.socket.on('connect_error', (error: any) => {
      this.connectionEmitter.emit('connect_error', { error });
    });

    this.socket.on('connect_timeout', (timeout: any) => {
      this.connectionEmitter.emit('connect_timeout', { timeout });
    });

    this.socket.on('disconnect', () => {
      // this.socket = undefined;
      // this.uid = undefined;
      // this.teamName = undefined;

      this.connectionEmitter.emit('disconnect');
    });

    this.socket.on('reconnect', async () => {
      console.log('reconnected');
      if (this.teamName) {
        await this.changeTeam(this.teamName);
      }
    });

    this.socket.on('peer_session_hanged', (hostId: string, sessionId: string) => {
      this.sessionsEmitter.emit('hangout', { hostId, sessionId });
    });

    this.socket.on('online_users', (uids: string[]) => {
      this.usersEmitter.emit('online_users', uids);
    });

    this.socket.on('check_programs_versions', (callback: (error: any, result?: any) => void) => {
      this.usersEmitter.emit('check_programs_versions', callback);
    });

    this.socket.on('peer_session_invite', (hostId: string, sessionId: string, terminalEntry: TerminalEntry) => {
      console.log('invite', hostId);
      this.sessionsEmitter.emit('invite', hostId, sessionId, terminalEntry);
    });

    this.socket.on('peer_session_accepted', (clientId: string, sessionId: string) => {
      console.log('accepted', sessionId);
      this.hangingSessionIds[sessionId] = undefined;
      this.sessionsEmitter.emit('accepted', clientId, sessionId);
    });

    this.socket.on('peer_session_rejected', (clientId: string, sessionId: string) => {
      console.log('rejected', sessionId);
      this.hangingSessionIds[sessionId] = undefined;
      this.closePeerSessionBySessionId(sessionId);
      this.sessionsEmitter.emit('rejected', clientId, sessionId);
    });

    return new Promise((resolve, reject) => {
      if (!this.socket) {
        this.uid = undefined;
        return reject('WS socket is undefined while connecting');
      }
      this.socket.once('connect', () => {
        this.uid = uid;
        console.log('Connected to multiplayer');
        return resolve();
      });
      this.socket.once('error', (error: any) => {
        this.uid = undefined;
        return reject(error);
      });
      this.socket.connect();
    });
  }
}


export { SessionsManager, TerminalEntry };
