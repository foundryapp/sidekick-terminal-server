import io from 'socket.io';
import * as lokijs from 'lokijs';
import { EventEmitter } from 'events';

interface TerminalEntry {
  tty: string;
  pid: number;
  cwd: string;
  socketId: string;
  terminalId: string;
  sessionId?: string;
  cols: number;
  rows: number;
  terminalIndex: number;
}

class TerminalsManager {
  private terminalServer: io.Namespace;
  private cliServer: io.Namespace;
  private server: io.Server;
  private db = new lokijs.default('terminals.db');
  private mark: undefined | true = undefined;
  private terminalIndex = 0;

  readonly terminals: lokijs.Collection<TerminalEntry>;

  terminalsEmitter = new EventEmitter();
  cliEmitter = new EventEmitter();

  constructor(private port: number) {
    this.terminals = this.db.addCollection<TerminalEntry>('terminals', { autoupdate: true, clone: true });

    this.server = io();
    this.terminalServer = this.server.of('/terminal');
    this.cliServer = this.server.of('/cli');

    this.terminalServer.on('connect', (socket) => {
      console.debug(`Terminal "${socket.id}" connected`);

      this.terminals.insert({
        socketId: socket.id,
        terminalId: socket.id,
        tty: socket.handshake.headers.tty,
        cols: socket.handshake.headers.cols,
        rows: socket.handshake.headers.rows,
        cwd: socket.handshake.headers.cwd,
        pid: socket.handshake.headers.pid,
        terminalIndex: this.terminalIndex++,
      });

      if (this.mark) {
        this.mark = undefined;
        socket.emit('mark');
      }

      socket.on('disconnect', () => {
        const terminal = this.terminals.findOne({ 'socketId': socket.id });
        if (terminal) {
          console.log(`Terminal "${socket.id}" disconnected`);
          this.terminalsEmitter.emit('disconnect', terminal.socketId);
          this.terminals.remove(terminal);
        }
      });
    });

    this.cliServer.on('connect', (socket) => {
      const { event }: { event: string } = socket.handshake.headers;
      switch (event) {
        case 'send_message':
          const { receiver_username, message }: { receiver_username: string, message: string } = socket.handshake.headers;
          // The last parameter of this EventEmitter is a response object that the receiver can edit as a response.
          this.cliEmitter.emit('send_message', { socketId: socket.id, receiverUsername: receiver_username, message });
          break;
      }
    });

    this.server.listen(this.port);
  }

  // This method is called from the sessionManager
  // TODO: Add type to response
  respondToCLI(socketId: string, status: number, response: any) {
    console.log('<><><> RESPONSE TO CLI', status, response);
    this.cliServer.connected[socketId]?.emit('response', { status, response });
  }

  async updateTerminalEntry(terminalId: string) {
    const terminal = this.terminals.findOne({ terminalId });
    if (terminal) {
      const update: { cols: number, rows: number } = await new Promise((resolve, reject) => {
        this.terminalServer.connected[terminal.socketId].emit('update-entry', (cols: number, rows: number) => {
          resolve({ cols, rows });
        });
      });

      terminal.rows = update.rows;
      terminal.cols = update.cols;
      this.terminals.update(terminal);
    }
  }


  turnTerminalHighlightOn(terminalId: string, message: string, color: string) {
    const terminal = this.terminals.findOne({ terminalId });
    if (terminal) {
      console.log('sending highlight on')
      this.terminalServer.connected[terminal.socketId].emit('highlight_on', message, color);
    } else {
      throw new Error(`Cannot find terminal with id ${terminalId}`);
    }
  }

  turnTerminalHighlightOff(terminalId: string) {
    const terminal = this.terminals.findOne({ terminalId });
    if (terminal) {
      this.terminalServer.connected[terminal.socketId].emit('highlight_off');
    } else {
      throw new Error(`Cannot find terminal with id ${terminalId}`);
    }
  }

  markNextConnectedTerminal() {
    this.mark = true;
  }

  closeAllPeerSessions() {
    this.terminalServer.emit('exit_peer_session');
    this.terminals.updateWhere(() => true, (doc) => {
      doc.sessionId = undefined;
    });
  }

  joinPeerSession(terminalId: string, sessionId: string) {
    const terminal = this.terminals.findOne({ terminalId });
    if (terminal) {
      if (terminal.sessionId) {
        throw new Error(`There is already an active session (${terminal.sessionId}) in terminal ${terminalId}`)
      }
      terminal.sessionId = sessionId;
      this.terminals.update(terminal);

      console.debug('Joining peer session');
      this.terminalServer.connected[terminal.socketId].emit('join_peer_session', sessionId);
    } else {
      console.debug(`Cannot find terminal with id ${terminalId}`);
    }
  }

  createPeerSession(terminalId: string, sessionId: string) {
    const terminal = this.terminals.findOne({ terminalId });
    if (terminal) {
      // if (terminal.sessionId) {
      //   throw new Error(`There is already an active session (${terminal.sessionId}) in terminal ${terminalId}`)
      // }
      terminal.sessionId = sessionId;
      this.terminals.update(terminal);

      this.terminalServer.connected[terminal.socketId].emit('create_peer_session', sessionId);

      console.debug('Creating peer session');
    } else {
      console.debug(`Cannot find terminal with id ${terminalId}`);
    }
  }

  exitPeerSession(terminalId: string) {
    const terminal = this.terminals.findOne({ terminalId });
    if (terminal) {
      terminal.sessionId = undefined;
      this.terminals.update(terminal);

      this.terminalServer.connected[terminal.socketId].emit('exit_peer_session');
    } else {
      throw new Error(`Cannot find terminal with id ${terminalId}`);
    }
  }
}


export { TerminalsManager, TerminalEntry };
