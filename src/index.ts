import { SessionsManager, TerminalEntry } from './sessionsManager';

async function debug() {
  const uid = process.argv[2];
  const teamName = process.argv[3];

  const manager = new SessionsManager();


  manager.usersEmitter.on('check_programs_versions', async (callback: (error: any, result?: any) => void) => {
  });

  manager.usersEmitter.on('online_users', (uids: string[]) => {
    console.log('Online users:', uids);
  });

  await manager.connect(uid);
  await manager.changeTeam(teamName);
}


if (require.main === module) {
  debug();
}


export { SessionsManager, TerminalEntry };
